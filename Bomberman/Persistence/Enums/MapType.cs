﻿namespace Persistence.Enums
{
    /// <summary>
    /// There are 3 map types and 1 tutorial
    /// </summary>
    public enum MapType
    {
        TYPE1,
        TYPE2,
        TYPE3,
        TUTORIAL
    }
}
