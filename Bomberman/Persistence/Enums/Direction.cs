﻿namespace Persistence.Enums
{
    /// <summary>
    /// 4 directions are given in which the objects can move
    /// </summary>
    public enum Direction
    {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }
}
