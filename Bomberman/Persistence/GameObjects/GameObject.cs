﻿namespace Persistence.GameObjects
{
    /// <summary>
    /// this class is the main parent of all of the game elements (it cannot be instanced directly)
    /// </summary>
    public abstract class GameObject {}
}
