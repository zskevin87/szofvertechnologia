# BomberMan game

This game was made as an assingment for a subject (SzoftverTechnológia) at my university (ELTE IK).
The project was made in a team called NobodyKnowsNobody. We were four in the team and we had to create a game, based on a description we got, using GitLab. 

We used Agile software development and we had scrums every week where we planned our next things to do until the next scrum, talked about our problems in the development and agreed in methods we have used in our software. Our scrum-master was our practice manager of the class.

The whole development was cut into 4 parts ("Milestones"):
- Plannings and previous works
- Implementation 1
- Implementation 2
- Unit tests, CI, fixing code

Every part had a deadline till we had to do the tasks (2-3 weeks for each part).
After the deadline, we had to present our work and what we have done so far.
We got then a mark for our job and we continued our work.

## Informations about the plannings and the previous works

At this part, we had to plan the whole software using UML.
We made lots of diagrams, such as package diagram, functional/unfunctional diagram, class diagram, etc.

The whole planning is available in the Wiki page of this project:
[Wiki Page](https://gitlab.com/szoftvertechnol-gia/szofvertechnologia/-/wikis/home)


## Informations about Prototype 1, 2 and 3.

Until the 2nd and the 3rd Milestones we had to implement our previous plannings.
The requirements were for examples Clean-coding, summaries in methods and 90% functional working until the 3rd deadline.

Until the 4th deadline, we had to present the whole functioning software with clean git usings, unittests, and so on.

See more details at the repository graph.

## Informations about the software

For coding we used Windows Presentation Foundation (WPF) framework, using C# and XAML. 
The architecture was Model-View-ViewModel (MVVM), so we separated the whole project into less parts: different parts for the persistence, the model, the viewmodel and the view (4 parts in total). Each part was coded in different projects (see the package diagram). 

## Authors

Thanks for the collaboration and job!
**Móricz Ádám**, **Zóka Zsolt László**, **Vajda Levente Zoltán**

Me: **Kevin Zsombók**
