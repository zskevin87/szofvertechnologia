﻿namespace Persistence.Enums
{
    /// <summary>
    /// Enum of different powerups
    /// </summary>
    public enum PowerUps
    {
        EXTRABOMBS,
        BIGGERBLAST,
        DETONATOR,
        ROLLERSKATE,
        INVINCIBILITY,
        GHOST,
        BARRIER,
        SLOWER,
        SMALLRANGE,
        NOBOMB,
        INSTANTBOMB
    }
}
